class Time

  VALIDATE_TIME_REGEX = /^(([0,1]?[0-9])|([2]?[0-3])):[0-5]?[0-9]:[0-5]?[0-9]$/

  def self.add(*time_list)
    total_seconds = 0
    if time_list.each do |time|
      if (time =~ VALIDATE_TIME_REGEX) == 0
        total_seconds += seconds(time)
      else
        break
      end
    end
      put(total_seconds)
    else
      puts "Invalid Input"
    end
  end

  def self.seconds(time)
    time = time.split(':').map { |value| value.to_i }
    time[0] * 60 * 60 + time[1] * 60 + time[2]
  end

  def self.put(time)
    seconds = time % 60
    time /= 60
    minutes = time % 60
    time /= 60
    hours = time % 24
    time /= 24
    days = time
    if days != 0
      printf("%02.0f day & %02.0f:%02.0f:%02.0f\n", days, hours, minutes, seconds)
    else
      printf("%02.0f:%02.0f:%02.0f\n", hours, minutes, seconds)
    end
  end
end

